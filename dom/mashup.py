# -*- encoding: utf-8 -*-

import requests
from pprint import pprint

#addr = raw_input('Podaj adres: ')

# Znalezienie współrzędnych adresu
base = 'http://maps.googleapis.com/maps/api/geocode/json'
addr = 'Nowoursynowska 159, Warszawa'
data = {'address': addr, 'sensor': 'false'}
result = requests.get(base, params=data)
# pprint(result.json())
lat = result.json()['results'][0]['geometry']['location']['lat']
lng = result.json()['results'][0]['geometry']['location']['lng']


# Znalezienie najbliższych geoskrytek
key = 'aLQ8aKXaHktr6Wz5g5mc'
base_geo = 'http://opencaching.pl/okapi/services/caches/search/nearest'
data2 = { 'center': str(lat)+ '|' +str(lng), 'consumer_key': key }
result2 = requests.get(base_geo, params=data2)
caches = '|'.join(result2.json()['results'])

# Wydobycie informacji o lokalizacji skrytek
base2 = 'http://opencaching.pl/okapi/services/caches/geocaches'
data3 = { 'cache_codes':caches, 'consumer_key': key }
result3 = requests.get(base2, params=data3)
r = result3.json()

# Przydzielenie adresu skrytkom na podstawie ich współrzędnych
skrzynki = []

for s in r:
    sk = r[s]
    geo = sk['location'].split('|')
    data_google = {'latlng':geo[0]+','+geo[1], 'sensor': 'false'}
    try:
        result4 = requests.get(base, params=data_google)
        adres = (result4.json()['results'][0]['formatted_address'])
        adresskrzynki = [adres, sk['name'], sk['status']]
        skrzynki.append(adresskrzynki)
    except:
        continue

# Wyświetlenie wyniku
for skrzynka in skrzynki:
    print skrzynka[0] + '|' + skrzynka[1] + '|' + skrzynka[2]