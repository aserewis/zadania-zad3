# -*- encoding: utf-8 -*-

import getpass
import requests
from bs4 import BeautifulSoup

url = {
    'login': 'https://canvas.instructure.com/login',
    'grades': 'https://canvas.instructure.com/courses/838324/grades'
}

username = raw_input('Użytkownik: ')
password = getpass.getpass('Hasło: ')

payload = {
    'pseudonym_session[unique_id]': username,
    'pseudonym_session[password]': password
}

# Logowanie
seassion = requests.session()
seassion.post('https://canvas.instructure.com/login', data=payload)

# Pobranie strony z ocenami
response = seassion.get('https://canvas.instructure.com/courses/838324/grades')

# Odczyt strony
html = response.text

# Parsowanie strony
parsed = BeautifulSoup(html)

# Scraping


# Sortowanie

# Wyświetlenie posortowanych ocen w kategoriach